$(document).ready(function(){
	$.ajax({
		url: "http://irigate.com/time-vs-temp.php",
		method: "GET",
		success: function(data) {
			console.log(data);
			var data_time = [];
			var temp = [];

			for(var i in data) {
				data_time.push(data[i].data_time);
				temp.push(data[i].temp);
			}

			var chartdata = {
				labels: data_time,
				datasets : [
					{
						label: "Time vs Temp ("+String.fromCharCode(176)+"c)",
						data: temp,
						backgroundColor: 'rgb(75, 192, 192)',
						borderColor: 'rgb(75, 192, 192)',
						fill: false,
						pointHoverRadius: 30
					}
				]
			};

			var ctx = $("#mycanvas");

			var lineGraph = new Chart(ctx, {
				type: 'line',
				data: chartdata,
				options: {
					responsive: true,
					legend: {
						position: 'bottom',
					},
					hover: {
						mode: 'index'
					},
					scales: {
						xAxes: [{
							display: true,
							scaleLabel: {
								display: true,
								labelString: 'Time'
							}
						}],
						yAxes: [{
							display: true,
							scaleLabel: {
								display: true,
								labelString: 'Temperature ('+String.fromCharCode(176)+'c)'
							}
						}]
					},
					title: {
						display: true,
						text: 'Time vs Temperature ('+String.fromCharCode(176)+'c)'
					}
				}
			});
		},
		error: function(data) {
			console.log(data);
		}
	});
});