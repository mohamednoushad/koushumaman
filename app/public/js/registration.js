

function validate (input) {
    if($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
        if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
            return false;
        }
    }
    else {
        if($(input).val().trim() == ''){
            return false;
        }
    }
}

function showValidate(input) {
    var thisAlert = $(input).parent();

    $(thisAlert).addClass('alert-validate');
}

function hideValidate(input) {
    var thisAlert = $(input).parent();

    $(thisAlert).removeClass('alert-validate');
}

$(document).ready(function(){ 


    $( "#register" ).click(function(event) {

        event.preventDefault();
        event.stopImmediatePropagation();
    
        $(thisAlert).removeClass('alert-login');
        $(thisAlert).removeClass('alert-metamask');
        $(thisAlert).removeClass('alert-credentials');
        $(thisAlert).removeClass('alert-error');
    
    
        let name = $("#name").val();
        let email = $("#email").val();
        let password = $("#password").val();

        var input = $('.validate-input .input100');

        for(var i=0; i<input.length; i++) {

            if(validate(input[i]) == false){

                showValidate(input[i]);    
 
                } else {
              
                var thisAlert = $(input).parent(); 
   
                $(thisAlert).removeClass('alert-validate');   

                }

            if(name && email && password) {
                          
                    $(thisAlert).removeClass('alert-login');
                    $(thisAlert).removeClass('alert-metamask');
                    $(thisAlert).removeClass('alert-credentials');
                    $(thisAlert).removeClass('alert-error');
    
                    let url = '/createUser';
    
                    let data = {
                        name:name,
                        email:email,
                        password : password
                    }
    
                    $.post(url,data,function(data){

                        console.log(data);

                        if(data === "user created successfully"){

                            $("#modalHeader2").html("User Succefully Registered");
                            $("#modelContent2").html("Please login !");
                            $("#myModal2").modal()

                        }else{

                            $("#modalHeader2").html("Error!");
                            $("#modelContent2").html("User Already Exist!");
                            $("#myModal2").modal()
                         
                        }
                              
                     });
    
              break;
                
            } else if (name && email && !password) {

                $(thisAlert).removeClass('alert-credentials');

                showValidate(input[3]);

            } else if (name && !email && password) {

                $(thisAlert).removeClass('alert-credentials');

                showValidate(input[1]);

            } else if (!name && email && password) {

                $(thisAlert).removeClass('alert-credentials');

                showValidate(input[0]);

            }else if(!name && email && !username && !password ) {
                $(thisAlert).removeClass('alert-credentials');

                showValidate(input[0]);
                showValidate(input[1]);
                showValidate(input[2]);
                showValidate(input[3]);

            }
       }            
    });


      
});