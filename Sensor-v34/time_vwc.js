$(document).ready(function(){
	$.ajax({
		url: "http://irigate.com/time-vs-vwc.php",
		method: "GET",
		success: function(data) {
			console.log(data);
			var data_time = [];
			var vwc = [];

			for(var i in data) {
				data_time.push(data[i].data_time);
				vwc.push(data[i].vwc);
			}

			var chartdata = {
				labels: data_time,
				datasets : [
					{
						label: "Time vs VWC",
						data: vwc,
						backgroundColor: 'rgb(75, 192, 192)',
						borderColor: 'rgb(75, 192, 192)',
						fill: false,
						pointHoverRadius: 30
					}
				]
			};

			var ctx = $("#mycanvastimevwc");

			var lineGraph = new Chart(ctx, {
				type: 'line',
				data: chartdata,
				options: {
					responsive: true,
					legend: {
						position: 'bottom',
					},
					hover: {
						mode: 'index'
					},
					scales: {
						xAxes: [{
							display: true,
							scaleLabel: {
								display: true,
								labelString: 'Time'
							}
						}],
						yAxes: [{
							display: true,
							scaleLabel: {
								display: true,
								labelString: 'VWC'
							}
						}]
					},
					title: {
						display: true,
						text: 'Time vs VWC'
					}
				}
			});
		},
		error: function(data) {
			console.log(data);
		}
	});
});