$(document).ready(function () {
	$.ajax({
		url: "http://irigate.com/temp-vs-vwc.php",
		method: "GET",
		success: function (data) {
			console.log(data);
			var temp = [];
			var vwc = [];

			for (var i in data) {
				temp.push(data[i].temp);
				vwc.push(data[i].vwc);
			}

			var chartdata = {
				labels: temp,
				datasets: [
					{
						label: "Temp (" + String.fromCharCode(176) + "c) vs VWC",
						data: vwc,
						backgroundColor: 'rgb(75, 192, 192)',
						borderColor: 'rgb(75, 192, 192)',
						fill: false,
						pointHoverRadius: 30
					}
				]
			};

			var ctx = $("#mycanvastempvwc");

			var lineGraph = new Chart(ctx, {
				type: 'line',
				data: chartdata,
				options: {
					responsive: true,
					legend: {
						position: 'bottom',
					},
					hover: {
						mode: 'index'
					},
					scales: {
						xAxes: [{
							display: true,
							scaleLabel: {
								display: true,
								labelString: 'Temperature ('+String.fromCharCode(176)+'c)'
							}
						}],
						yAxes: [{
							display: true,
							scaleLabel: {
								display: true,
								labelString: 'VWC'
							}
						}]
					},
					title: {
						display: true,
						text: 'Temperature ('+String.fromCharCode(176)+'c) vs Volumetric Water Content'
					}
				}
			});
		},
		error: function (data) {
			console.log(data);
		}
	});
});