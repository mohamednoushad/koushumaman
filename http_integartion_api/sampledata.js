{ applicationID: '2',
  applicationName: 'soilwcs3',
  deviceName: 'SoilSensor1',
  devEUI: '1a0d582088813d7a',
  txInfo: { frequency: 868500000, dr: 5 },
  adr: true,
  fCnt: 16178,
  fPort: 1,
  data: 'AH4AAAepAABo',
  object: { bat: 104, e25: 1.26, ec: 0, temp: 19.61, vwc: 0 } }


  { applicationID: '1',
  applicationName: 'SensSwitch',
  deviceName: 'SensSwitch2',
  devEUI: '3c42089e2680f4e6',
  txInfo: { frequency: 868100000, dr: 5 },
  adr: true,
  fCnt: 170,
  fPort: 1,
  data: 'ATcAjQABBoAAAA==',
  object: { bat: 55, e25: 1.41, ec: 0.01, temp: 16.64, valv: 0, vwc: 0 } }