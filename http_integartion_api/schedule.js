
const schedule = require('node-schedule');
const lora = require('./loraApi');
var db = require('./mysql');
var influxDb = require('./influxDb');


// var time = new Date();
// console.log("server start time",time);

 function scheduler(jobName,hour,minutes,second,runTime,switchDevEui,sensorDevEui) {
       

    //console.log("inside scheduler function",jobName,hour,minutes,second,runTime)


    var a = runTime.split(':'); 
    var runTimeinSeconds = (+a[0]) *  60 * 60 + (+a[1]) * 60 + (+a[2]);

    var time = new Date();
    console.log("present time",time);
    console.log(minutes,second);

    db.addJob(jobName,sensorDevEui,switchDevEui,runTimeinSeconds,hour,minutes)
    .then(function(){
        //schedule.scheduleJob(jobName,{hour: hour, minute: minutes, second:second}, function(y){ //make to this after
            schedule.scheduleJob(jobName,{second:second}, function(y){
            console.log("this is jobName",y);
            db.getJob(y)
            .then(function(result){
                
                presentTime = new Date();
                console.log("trying to start job ",presentTime, presentTime.getUTCHours(),result[0].hour, presentTime.getUTCMinutes(),minutes);
                console.log(presentTime.getUTCHours() == result[0].hour);
                if(presentTime.getUTCHours() == result[0].hour && presentTime.getUTCMinutes() == result[0].minutes) {

                console.log("here data of job",result);

                let startTime = new Date(Date.now());
                let endTime = new Date(startTime.getTime() + result[0].run_time_in_seconds * 1000);

                var time = new Date();
                console.log("job initial start time",time);
                
                
                schedule.scheduleJob({start: startTime, end: endTime, rule: '*/20 * * * * *'}, function(){
                 
                       //function to switch off switch after ON ing
                    let finalEndTime = new Date(endTime.getTime() + 5000);
                    schedule.scheduleJob({start: endTime, end: finalEndTime, rule: '* * * * * *'}, function(){
                        console.log("job to OFF the switch");
                        //get value from db the last value of switch
                        db.getLastState(result[0].switch_dev_eui)
                        .then(function(lastSentValue){
                            //if ON --> true, run the close switch command
                            if(lastSentValue){
                                lora.closeSwitch(result[0].switch_dev_eui)
                                .then(function(status){
                                    console.log(status);
                                    console.log("switch closed");
                                    db.setLastStateToOFF(result[0].switch_dev_eui)
                                       .then(function(setState){
                                                console.log("Updated DB setting Last state to OFF");
                                        })
                                })
                            } else {
                                //else dont do anything
                                console.log("Already OFF ! No need to send Close Switch Command");

                            }
                        });

                    });

                //function logic to switch ON the switch
                var time = new Date();
                console.log("job interval running start time",time);
                    lora.isSensorActive(sensorDevEui)
                        .then(function(response){
                            if(response){ 
                                console.log("sensor active");
                                console.log(result);
                                influxDb.getLastValue(result[0].sensor_dev_eui)
                                    .then(function(value){
                                        console.log(value);
                                        db.getCutOffValue(result[0].switch_dev_eui)
                                            .then(function(cutOffVal){
                                                console.log(cutOffVal);
                                                    if(value<cutOffVal){
                                                        console.log("run job");
                                                        //get value from db for the last state of switch
                                                        db.getLastState(result[0].switch_dev_eui)
                                                        .then(function(lastSentValue){
                                                            //true for ON and false for OFF
                                                            //if OFF --> false, run open switch command
                                                            if(!lastSentValue) {
                                                                var new_time = new Date();
                                                                console.log(new_time);
                                                                lora.openSwitch(result[0].switch_dev_eui)
                                                                .then(function(status){
                                                                    console.log("switch opened");
                                                                    //Update DB value of last state to ON;
                                                                    db.setLastStateToON(result[0].switch_dev_eui)
                                                                    .then(function(setState){
                                                                        console.log("Updated DB setting Last state to ON");
                                                                    })
                                                                })
                                                            } else {
                                                                //if ON, dont do anything
                                                                console.log("Already ON ! No need to send Open Switch Command");
                                                               
                                                            }
                                                            
                                                        })
                                                
                                                    }else{
                                                            //dont do job
                                                            console.log("dont run job because sensor given value is greater than cut off value");
                                                            //get value from db the last value of switch
                                                            db.getLastState(result[0].switch_dev_eui)
                                                            .then(function(lastSentValue){
                                                                //if ON --> true, run the close switch command
                                                                if(lastSentValue){
                                                                    lora.closeSwitch(result[0].switch_dev_eui)
                                                                    .then(function(status){
                                                                        console.log(status);
                                                                        console.log("switch closed");
                                                                        db.setLastStateToOFF(result[0].switch_dev_eui)
                                                                           .then(function(setState){
                                                                                    console.log("Updated DB setting Last state to OFF");
                                                                            })
                                                                    })
                                                                } else {
                                                                    //else dont do anything
                                                                    console.log("Already OFF ! No need to send Close Switch Command");

                                                                }
                                                            });
                                                    }
                                            });
                                    });                        

                            } else {

                                console.log("sensor is inactive");
                                console.log("run job, open switch");
                                //get value from db for the last state of switch

                                db.getLastState(result[0].switch_dev_eui)
                                .then(function(lastSentValue){
                                    //true for ON and false for OFF
                                    //if OFF --> false, run open switch command
                                    if(!lastSentValue) {
                                        var new_time = new Date();
                                        console.log(new_time);
                                        lora.openSwitch(result[0].switch_dev_eui)
                                        .then(function(status){
                                            console.log("switch opened");
                                            //Update DB value of last state to ON;
                                            db.setLastStateToON(result[0].switch_dev_eui)
                                            .then(function(setState){
                                                console.log("Updated DB setting Last state to ON");
                                            })
                                        })
                                    } else {
                                        //if ON, dont do anything
                                        console.log("Already ON ! No need to send Open Switch Command");
                                                                              
                                    }

                                });
                              
                            }

                        });

                });
                } else {
                    console.log("not this hour because present UTC hour and minute is "+ presentTime.getUTCHours(),presentTime.getUTCMinutes() +" while actual job time is "+ result[0].hour,result[0].minutes);
                }
            });

        }.bind(null,jobName));

            
     });
 }




module.exports = {


    scheduleJob : async function(jobStartTime,jobRunTime,switchDevEui,sensorDevEui) {


        if(jobStartTime.length > 1) {     //for scheduling two jobs

            for(const i in jobStartTime) {
     
        
                let jobName = switchDevEui+"_"+i;

                schedule.cancelJob(jobName); //cancel the job if already existing

                jobStart = jobStartTime[i];
                jobRun = jobRunTime[i];

                let time = jobStart.split(":");
                let hour = time[0];
                let minutes = time[1];
                let seconds = time[2];

                //console.log(jobName,hour,minutes,seconds,jobRun,switchDevEui,sensorDevEui);


                scheduler(jobName,hour,minutes,seconds,jobRun,switchDevEui,sensorDevEui);
            
                
            }

            

        } else if (jobStartTime.length == 1) {  // for scheduling one job

          
            let jobName = switchDevEui+"_0";

            schedule.cancelJob(jobName); //cancel the job if already existing

            jobStart = jobStartTime[0];
            jobRun = jobRunTime[0];

            let time = jobStart.split(":");
            let hour = time[0];
            let minutes = time[1];
            let seconds = time[2];

            scheduler(jobName,hour,minutes,seconds,jobRun,switchDevEui,sensorDevEui);


        } else {

             //console.log("do nothing");
        }
 
    },

    scheduleSecond : function() {

        console.log("here");

        let startTime = new Date(Date.now() + 5000);
        let endTime = new Date(startTime.getTime() + 5000);

        var j = schedule.scheduleJob({start: startTime, end: endTime, rule: '* * * * * *'}, function(){
            console.log('Time for tea!');
                var new_time = new Date();
               console.log(new_time);

        });

        // j.cancel();


        // var rule = new schedule.RecurrenceRule();
        // rule.second = 4;
        
        // var j = schedule.scheduleJob(rule, function(){
        // console.log('The answer to life, the universe, and everything!');
        // var new_time = new Date();
        // console.log(new_time);
        // });



    }


}


//  var rule = new schedule.RecurrenceRule();
//         rule.second = 4;
        
//         var j = schedule.scheduleJob(rule, function(){
//         console.log('The answer to life, the universe, and everything!');
//         var new_time = new Date();
//         console.log(new_time);
//         });

//         rule.second = 6;
//         var j = schedule.scheduleJob(rule, function(){
//             console.log('The answer to life, the universe, and everything!');
//             var new_time = new Date();
//             console.log(new_time);
//             });

//             rule.second = 10;
//             var j = schedule.scheduleJob(rule, function(){
//                 console.log('The answer to life, the universe, and everything!');
//                 var new_time = new Date();
//                 console.log(new_time);
//                 });
        


//  schedule.scheduleJob("noush",{second:10}, function(){
//     var new_time = new Date();
//     console.log(new_time);
//     console.log('Time for First tea!');


//     let startTime = new Date(Date.now() + 5000);
//     let endTime = new Date(startTime.getTime() + 5000);

//     var j = schedule.scheduleJob({start: startTime, end: endTime, rule: '* * * * * *'}, function(){
//         console.log('Time for Second tea!');
//             var new_time = new Date();
//            console.log(new_time);
          

//     });
//   });

  

//  var  j = schedule.scheduleJob({second:20}, function(){
//     var new_time = new Date();
//     console.log(new_time);
//     console.log('Time for tea!');
//   });

//  var  j = schedule.scheduleJob({second:30}, function(){
//     var new_time = new Date();
//     console.log(new_time);
//     console.log('Time for tea!');
//   });

//   j.cancel();



  //console.log(schedule.scheduledJobs) 

//schedule.cancelJob("noush");

    // let startTime = new Date(Date.now() + 5000);
    // let endTime = new Date(startTime.getTime() + 3 * 60 * 1000);
     
    // var time = new Date();
    // console.log("server start time",time);

    // var j = schedule.scheduleJob({start: startTime, end: endTime, rule: '1 * * * * *'}, function(){
    //     console.log("call valve and check last value as well");
    //     console.log('Time for Second tea!');
    
    //         var new_time = new Date();
    //        console.log(new_time);

    // });

 

//  let x = 'Tada!';

//   var j =  schedule.scheduleJob("noushad",{second:50}, function(){
        
//         var new_time = new Date();
//         console.log(new_time);
//         console.log(x);
//     }.bind(x));

    
// var x = 'Tada!';
// schedule.scheduleJob("noushad",{second:10}, function(y){
//   console.log("this is x",y);
// }.bind(null,x));
// x = 'Changing Data';

        // var new_time = new Date();
        // console.log(new_time);


    