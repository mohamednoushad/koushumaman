-- MySQL dump 10.13  Distrib 5.7.25, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: users
-- ------------------------------------------------------
-- Server version	5.7.25-0ubuntu0.16.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `device_jobs`
--

DROP TABLE IF EXISTS `device_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_jobs` (
  `job_name` varchar(60) NOT NULL,
  `sensor_dev_eui` varchar(45) DEFAULT NULL,
  `switch_dev_eui` varchar(45) DEFAULT NULL,
  `run_time_in_seconds` int(11) DEFAULT NULL,
  `hour` int(11) DEFAULT NULL,
  `minutes` int(11) DEFAULT NULL,
  PRIMARY KEY (`job_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_jobs`
--

LOCK TABLES `device_jobs` WRITE;
/*!40000 ALTER TABLE `device_jobs` DISABLE KEYS */;
INSERT INTO `device_jobs` VALUES ('3c42089e2680f4e6_0','1a0d582088813d7a','3c42089e2680f4e6',3600,9,0),('3c42089e2680f4e6_1','1a0d582088813d7a','3c42089e2680f4e6',3601,11,0),('b635739d5bb4eee5_0','b635739d5bb4eee5','b635739d5bb4eee5',120,16,49),('b635739d5bb4eee5_1','b635739d5bb4eee5','b635739d5bb4eee5',300,17,0);
/*!40000 ALTER TABLE `device_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_credential`
--

DROP TABLE IF EXISTS `user_credential`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_credential` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) DEFAULT NULL,
  `name` varchar(60) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_credential`
--

LOCK TABLES `user_credential` WRITE;
/*!40000 ALTER TABLE `user_credential` DISABLE KEYS */;
INSERT INTO `user_credential` VALUES (3,'mytrials2017@gmail.com','noushad','7c4a8d09ca3762af61e59520943dc26494f8941b'),(4,'admin@admin.com','admin','d033e22ae348aeb5660fc2140aec35850c4da997');
/*!40000 ALTER TABLE `user_credential` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_devices`
--

DROP TABLE IF EXISTS `user_devices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_devices` (
  `device_eui` varchar(60) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `device_name` varchar(60) DEFAULT NULL,
  `application_id` int(11) DEFAULT NULL,
  `device_type` varchar(45) DEFAULT NULL,
  `first_start_time` time DEFAULT NULL,
  `first_run_time` time DEFAULT NULL,
  `cut_off_value` varchar(45) DEFAULT NULL,
  `device_run_order` int(11) DEFAULT NULL,
  `second_start_time` time DEFAULT NULL,
  `second_run_time` time DEFAULT NULL,
  `cut_off_device` varchar(60) DEFAULT NULL,
  `last_state` varchar(45) DEFAULT 'OFF',
  PRIMARY KEY (`device_eui`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_devices`
--

LOCK TABLES `user_devices` WRITE;
/*!40000 ALTER TABLE `user_devices` DISABLE KEYS */;
INSERT INTO `user_devices` VALUES ('1a0d582088813d7a',3,'SoilSensor1',2,'sensor',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'OFF'),('3c42089e2680f4e6',3,'SensSwitch2',1,'switch','09:00:00','01:00:00','2',1,'11:00:00','01:00:01','1a0d582088813d7a','OFF'),('b635739d5bb4eee5',3,'SensSwitch1',1,'both','16:49:00','00:02:00','1',2,'17:00:30','00:05:00','b635739d5bb4eee5','OFF'),('dsfd',4,'DummySwitch1',NULL,'switch',NULL,NULL,NULL,2,NULL,NULL,NULL,'OFF'),('dsfds',4,'DummySwitch2',NULL,'switch',NULL,'00:00:00',NULL,1,NULL,NULL,NULL,'OFF');
/*!40000 ALTER TABLE `user_devices` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-03-21 22:40:09
