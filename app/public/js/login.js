




function validate (input) {
    if($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
        if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
            return false;
        }
    }
    else {
        if($(input).val().trim() == ''){
            return false;
        }
    }
}

function showValidate(input) {
    var thisAlert = $(input).parent();

    $(thisAlert).addClass('alert-validate');
}

function hideValidate(input) {
    var thisAlert = $(input).parent();

    $(thisAlert).removeClass('alert-validate');
}





$(document).ready(function(){ 


    $( "#login" ).click(function(event) {

        event.preventDefault();
        event.stopImmediatePropagation();
    
        $(thisAlert).removeClass('alert-login');
        $(thisAlert).removeClass('alert-metamask');
        $(thisAlert).removeClass('alert-credentials');
        $(thisAlert).removeClass('alert-error');
    
    
        let email = $("#email").val();
        let password = $("#password").val();
    
     
        var input = $('.validate-input .input100');

     
    
        for(var i=0; i<input.length; i++) {
            if(validate(input[i]) == false){
                showValidate(input[i]);    
                          //no input
                }else{
              
                var thisAlert = $(input).parent(); 
            

                $(thisAlert).removeClass('alert-validate');   //input present and checking if both username and password is admin
                }

            if(email && password) {
           

    
                    $(thisAlert).removeClass('alert-login');
                    $(thisAlert).removeClass('alert-metamask');
                    $(thisAlert).removeClass('alert-credentials');
                    $(thisAlert).removeClass('alert-error');
    
                    let url = '/login';

                  
                    let data = {
                        email : email,
                        password : password,
                        }

                    $.ajaxSetup({
                        crossDomain: true,
                        xhrFields: {
                            withCredentials: true
                        }
                    });
    
                    $.post(url,data,function(response) {

                        console.log(response);

                        if(response=="Invalid credentials.") {

                            $(thisAlert).addClass('alert-credentials');

                            // showValidate(input[0]);
                            // showValidate(input[1]);

                        } else {

                            window.location.href = "/authrequired";

                        } 
                  
                    });
                   
          
                break;
                
            } else if (email && !password) {

                $(thisAlert).removeClass('alert-credentials');

                showValidate(input[1]);

            } else if (!email && password) {

                $(thisAlert).removeClass('alert-credentials');

                showValidate(input[0]);

            } else if(!email && !password) {
                $(thisAlert).removeClass('alert-credentials');

                showValidate(input[0]);
                showValidate(input[1]);


            }

       }            
 
    });

 });