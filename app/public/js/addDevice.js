console.log("here");

let deviceType;


$(".dropdown-item").click(function(){

    deviceType = $(this).text();

    $("#displayDrop").html(deviceType);

});



window.addDevice = function() {

    let appId = $("#appId").val();
    let desc = $("#appId").val();
    let devEui = $("#devEui").val();
    let devProfId = $("#profId").val();
    let name = $("#deviceName").val();


    if(deviceType && appId && desc && devEui && devProfId && name) {
       
        let data =  {
    
            appId : appId,
            desc : desc,
            devEui : devEui,
            devProfId : devProfId,
            name : name,
            deviceType : deviceType
        
        }


      
    fetch('/device',{method: "POST",
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)})
          .then( response => {
            console.log("first response of fetch",response);
          
            console.log(response);

            return response;

          })
          .then(response => response.json())
          .then(parsedResponse => { 
        
              if(parsedResponse.message == "success") {


                $('#exampleModalLabel').html("Success");

                $('#fillDetails').modal('show');


              }else {

                  $('#exampleModalLabel1').html("Error : "+parsedResponse.error.message);

                  $('#fillDetails').modal('show');
              }
          });

    } else {

        $('#exampleModalLabel1').html("Please Fill All Details");

        $('#fillDetails').modal('show');

 
    }


}


