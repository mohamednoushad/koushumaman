var express = require('express')
var bodyParser = require('body-parser')
var app = express();
var scheduler = require('./schedule');

var db = require('./mysql');

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
 
// parse application/json
app.use(bodyParser.json())


async function getDevicesForJob() {

    const devices = await db.getDevicesHavingJob();

    //console.log("My SQL LIST OF DEVICES",devices);

    for (const i in devices) {

        element = devices[i];

        switchDevEui = element.device_eui;
        sensorDevEui = element.cut_off_device;

        let jobStartTimes = [];
        let jobRunTime = [];
        
        if(element.first_start_time && element.second_start_time) {

            //if there are two start times for the switch

            if(element.first_start_time == element.second_start_time) {

                //if start time for both the job is same

                jobStartTimes.push(element.first_start_time);
                jobRunTime.push(element.first_run_time);
                
            } else{

                //if start time for both the job is different

                jobStartTimes.push(element.first_start_time,element.second_start_time);
                jobRunTime.push(element.first_run_time,element.second_run_time);

            }

           


        } else if (element.first_start_time && ! element.second_start_time) {

            //if device has only first start time

            // console.log("device has only one first start time");
            jobStartTimes.push(element.first_start_time);
            jobRunTime.push(element.first_run_time);

        } else if (!element.first_start_time && element.second_start_time) {

            //if device has only second start time

            // console.log("only second start time");
            jobStartTimes.push(element.second_start_time);
            jobRunTime.push(element.second_run_time);

        } else {
            // console.log("no start time present");
        }

        // console.log(jobStartTimes);
        // console.log(jobRunTime);
        // console.log(devEui);

        //console.log("In main server js, job start time. job run time, switch device eui , sensor device eui are",jobStartTimes, jobRunTime, switchDevEui, sensorDevEui);

        const result  = await scheduler.scheduleJob(jobStartTimes,jobRunTime,switchDevEui,sensorDevEui);

        
        
    }
    
    // });


}

 
app.get('/', function (req, res) {
  res.send('Hello World')
})

app.get('/resheduleJobs', function (req, res) {

    getDevicesForJob();

    res.send({message:"done"});


      })
 
app.post('/uplink', function (req, res) {
    
    console.log("calling uplink api");
    console.log(req.body);

    // res.send('Hello World')
})

app.post('/join', function (req, res) {
    
    console.log("calling join api");
    console.log(req.body);

    // res.send('Hello World')
})

app.post('/status', function (req, res) {
    
    console.log("calling status api");
    console.log(req.body);

    // res.send('Hello World')
})

app.post('/location', function (req, res) {
    
    console.log("calling location api");
    console.log(req.body);

    // res.send('Hello World')
})

app.post('/ack', function (req, res) {
    
    console.log("calling acknowledgment api");
    console.log(req.body);

    // res.send('Hello World')
})

app.post('/error', function (req, res) {
    
    console.log("calling error api");
    console.log(req.body);

    // res.send('Hello World')
})





app.listen(4200)