let deviceNameChanged;
let durationChanged;
let graphData;


async function uniqueData(deviceName,duration,graphType) {

    $("#deviceName").html(deviceNameChanged);

    let data =  {
  
      device : deviceName ? deviceName : deviceNameChanged,
      searchTime : duration ? duration : durationChanged,
      graph : graphType ? graphType : graphData
  
      }


      switch(graphData) {
        case "Battery":
            unit = " %";
            break;
        default:
            unit = " ";
            break;        

      }
  
  
  
    fetch('/getSingleData',{method: "POST",
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)})
        .then( response => {
          console.log("first response of fetch",response);
          if (response.status !== 200) {
            console.log(response);
          }
          return response;
        })
        .then(response => response.json())
        .then(parsedResponse => {
          console.log("parsed response of fetch",parsedResponse);
          parsedResponse = parsedResponse;
          unpackData = (arr, key) => {
            return arr.map(obj => obj[key])
          }

          changeToLocalTime = (arr,key) => {
            return arr.map(obj=>new Date(obj[key]));
          }
  
          console.log("here",unpackData(parsedResponse, 'time'));
  
         const firstTrace = {
            type: 'scatter',
            mode: 'lines',
            name: 'e25',
            x: changeToLocalTime(parsedResponse, 'time'),
            y: unpackData(parsedResponse, 'value'),
            line: {color: '#17BECF'}
          }
      
          const data = [firstTrace];
          const layout = {
            title: graphData,
            yaxis: {
              ticksuffix: unit
            }
          };
          return Plotly.newPlot('graphs-container', data, layout,{displayModeBar: true});
        })
  
      }

    function getData(_this) {

        deviceName = $(_this).attr("value");
        deviceNameChanged = $(_this).attr("value");
        durationChanged = 5 ;

        $("#deviceName").html(deviceNameChanged);
    
        uniqueData(deviceName,durationChanged,graphData);
      
    }

    $(".category").click(function(){

        graphData = $(this).text();
    
        console.log($(this).text());
           
         uniqueData() 
   
          
    });

    $(".time").click(function(){

        console.log($(this).text());

   

        let inputTime = $(this).text();
        let convertedTimeinMinutes;
    
        switch(inputTime) {
          case "5 Minutes":
            convertedTimeinMinutes = 5;
            break;
          case "1 Hour":
            convertedTimeinMinutes = 60;
            break;
          case "2 Hour":
            convertedTimeinMinutes = 120;
            break;
          case "1 Day":
            convertedTimeinMinutes = 1440;
            break;
          case "10 Days":
            convertedTimeinMinutes = 14400;
            break;
          case "1 Month":
            convertedTimeinMinutes = 43800;
            break;
          case "1 Year":
            convertedTimeinMinutes = 525600;
            break;
    
          default:
          convertedTimeinMinutes =5;
          break;
        
        }

        console.log(convertedTimeinMinutes);
    
        durationChanged = convertedTimeinMinutes;
    
        console.log("calling drop down of time");
    
      
          uniqueData() 
        
     });


function loadMap() {
    var data = [{
        type:'scattermapbox',
        lat:['24.466667'],
        lon:['54.366669'],
        mode:'markers',
        marker: {
          size:14
        },
        text:['Al Manhal']
      }]
      
      var layout = {
        autosize: true,
        hovermode:'closest',
        mapbox: {
          bearing:0,
          center: {
            lat:24,
            lon:54
          },
          pitch:0,
          zoom:5
        },
      }
      
      Plotly.setPlotConfig({
        mapboxAccessToken: 'pk.eyJ1IjoibmF1c2hhZG1vaGFtZWQyMDAxIiwiYSI6ImNqc2h1bnhseTIwZGczem1sd3V3ZmhiYXAifQ.2k3ufGwKVRuWJy9XOssI6w'
      })
      
      Plotly.plot('maps-container', data, layout)
}






$(document).ready(function() {

    fetch('/listSwitches')
    .then( response => {
      return response;
    })
    .then(response => response.json())
    .then(parsedResponse => {
        console.log("here",parsedResponse);
      parsedResponse.forEach(element => {
        $(".collapse-header-switches").append('<a class="collapse-item" value="'+element.device_name+'" id="'+element.device_name+'" onclick="getData(this)" >'+element.device_name+'</a>');
      });
      deviceNameChanged = parsedResponse.length ? parsedResponse[0].device_name : null;
      if(deviceNameChanged == null) {
        $('#noDevicesModal').modal('show');
      } else {
      durationChanged = 5;
      graphData = "valve"
  
      uniqueData();
      setInterval(uniqueData,20*1000);

      loadMap();

      }
   });


    

});