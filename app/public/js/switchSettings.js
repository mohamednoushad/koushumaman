let deviceNameChanged;
let frequency;
let sensor;








function getData(_this) { //this is actually setting the switch


    deviceNameChanged = $(_this).attr("value");
   

    $("#deviceName").html(deviceNameChanged);


  
}

function setFrequency(_this) {


    frequency = $(_this).attr("value");
    let displayAboutFrequency;

    console.log(frequency);

    switch (frequency) {
        case "1":
            displayAboutFrequency = "First Time in Day";
            break;
        case "2":
            displayAboutFrequency = "Second Time in Day";
            break;
 
    }
   

    $("#freq").html(displayAboutFrequency);


  
}

function setSensor(_this) {


  sensor = $(_this).attr("value");
  
  console.log(sensor);

}

window.addTimeSettings = function () {

    let startTime = $("#inputStartTime").val();
    let runTime = $("#runTime").val();
    let cutOffVal = $("#cutOffVwcValue").val();
    let order = $("#order").val();

    console.log(startTime,runTime,cutOffVal,order);

    if(deviceNameChanged && frequency && startTime && runTime && cutOffVal) {

    $("#message").empty();

    let data =  {
  
        device : deviceNameChanged,
        frequency : frequency,
        startTime : startTime,
        runTime : runTime,
        cutOffVal : cutOffVal,
        order : order,
        sensorEui : sensor
       
        }
      
    fetch('/setTimer',{method: "POST",
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)})
          .then( response => {
            console.log("first response of fetch",response);
          
            console.log(response);

            return response;

          })
          .then(response => response.json())
          .then(parsedResponse => { 
              console.log(parsedResponse);
              if(parsedResponse.message == "success") {

                $("#deviceName").empty();
                $("#freq").empty();

                $("#message").html("Timer Successfully Updated");

              }
          });

    } else {

        $("#deviceName").empty();
        $("#freq").empty();

        $("#message").html("Please Fill All Details");

    }


}





$(document).ready(function() {

    fetch('/listSwitches')
    .then( response => {
      return response;
    })
    .then(response => response.json())
    .then(parsedResponse => {
        console.log(parsedResponse);
      parsedResponse.forEach(element => {
        $(".switches-menu").append('<a class="dropdown-item" value="'+element.device_name+'" id="'+element.device_name+'" onclick="getData(this)" >'+element.device_name+'</a>');
      });

      deviceNameChanged = parsedResponse.length ? parsedResponse[0].device_name : null;
      if(deviceNameChanged == null) {
        $('#noDevicesModal').modal('show');
      } 

     fetch('/listSensors')
     .then( response => {
       return response;
       })
       .then(response => response.json())
       .then(parsedResponse => {
         console.log("here",parsedResponse);
         parsedResponse.forEach(element => {
          $(".sensor-menu").append('<a class="dropdown-item" value="'+element.device_eui+'" id="'+element.device_name+'" onclick="setSensor(this)" >'+element.device_name+'</a>');
        });
       });

    });


    deviceNameChanged = "SensSwitch1";
  

});