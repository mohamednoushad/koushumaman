
let deviceNameChanged;
let durationChanged;
let singleDataFlag;
let graphData;


function mainfunction() {

  if(singleDataFlag) {

    uniqueData();

  } else {

    fetchData();

  }

}



async function uniqueData(deviceName,duration,graphType) {

  let data =  {

    device : deviceName ? deviceName : deviceNameChanged,
    searchTime : duration ? duration : durationChanged,
    graph : graphType ? graphType : graphData

    }



  fetch('/getSingleData',{method: "POST",
    headers: {
      'Accept': 'application/json, text/plain, */*',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)})
      .then( response => {
        console.log("first response of fetch",response);
        if (response.status !== 200) {
          console.log(response);
        }
        return response;
      })
      .then(response => response.json())
      .then(parsedResponse => {
        console.log("parsed response of fetch",parsedResponse);
        parsedResponse = parsedResponse;
        unpackData = (arr, key) => {
          return arr.map(obj => obj[key])
        }

        console.log("here",unpackData(parsedResponse, 'time'));

       const firstTrace = {
          type: 'scatter',
          mode: 'lines',
          name: 'e25',
          x: unpackData(parsedResponse, 'time'),
          y: unpackData(parsedResponse, 'value'),
          line: {color: '#17BECF'}
        }
    
        const data = [firstTrace];
        const layout = {
          title: graphData,
        };
        return Plotly.newPlot('graphs-container', data, layout,{displayModeBar: true});
      })

    }

async function fetchData(deviceName,duration) {

  let data =  {

    device : deviceName ? deviceName : deviceNameChanged,
    searchTime : duration ? duration : durationChanged

    }

  $.post( "/api", data)
  .done(function( result ) {

    const layout = {
      title: 'All Data',
    };

    return Plotly.newPlot('graphs-container', result, layout);

    });

 }
  


function getData(_this) {

    deviceName = $(_this).attr("value");
    deviceNameChanged = $(_this).attr("value");
    durationChanged = 5 ;

    if(singleDataFlag) {

      uniqueData(deviceName,durationChanged,graphData);
  
    } else {
  
      fetchData(deviceName,durationChanged);
  
    }

  }



  $(".dropdown-item").click(function(){

   

    let inputTime = $(this).text();
    let convertedTimeinMinutes;

    switch(inputTime) {
      case "5 minutes":
        convertedTimeinMinutes = 5;
        break;
      case "1 hour":
        convertedTimeinMinutes = 60;
        break;
      case "2 hour":
        convertedTimeinMinutes = 120;
        break;
      case "1 Day":
        convertedTimeinMinutes = 1440;
        break;
      case "10 Days":
        convertedTimeinMinutes = 14400;
        break;
      case "1 Month":
        convertedTimeinMinutes = 43800;
        break;
      case "1 Year":
        convertedTimeinMinutes = 525600;
        break;

      default:
      convertedTimeinMinutes =5;
      break;
    
    }

    durationChanged = convertedTimeinMinutes;

    console.log("calling drop down of time");

    if(singleDataFlag) { 

      uniqueData() 

    } else {

      fetchData();

    }

 });



 $(".category").click(function(){

  graphData = $(this).text();

  if(singleDataFlag) { 

    uniqueData() 

  }

});



$(function() {
  $('#toggle-event').change(function() {
    console.log(!$(this).prop('checked'));
    singleDataFlag = !$(this).prop('checked');

    if(singleDataFlag) { 

      uniqueData() 

    } else {

      fetchData();

    }
  
  })
})

  $(document).ready(function() {


    deviceNameChanged = "SoilSensor1";
    durationChanged = 5;
    graphData = "e25";
    singleDataFlag = true;


    fetch('/devices')
      .then( response => {
        return response;
      })
      .then(response => response.json())
      .then(parsedResponse => {
        parsedResponse.forEach(element => {
          $(".devices").append('<a class="dropdown-item" value="'+element+'" id="'+element+'" onclick="getData(this)" >'+element+'</a>');
        });
      });
      mainfunction();
      setInterval(mainfunction,20*1000);

});
