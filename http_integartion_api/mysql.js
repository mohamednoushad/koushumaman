const mysql  = require('mysql');


//local
// var connection = mysql.createConnection({
//     host     : '127.0.0.1',
//     user     : 'root',
//     password : '123456',
//     database : 'users'
//   });

//server
var connection = mysql.createConnection({
    host     : '172.17.0.3',
    user     : 'root',
    password : 'kausans2019',
    database : 'users'
  });




connection.connect();


module.exports = {

    getDevicesHavingJob : async function() {

        var sql = "SELECT * FROM users.user_devices WHERE device_type = 'both' OR device_type =  'switch'  ORDER BY  user_id , device_run_order" ;

        return new Promise((resolve, reject) => {
    
            connection.query(sql,async function(error,result){
       
                if(result){
    
                    resolve (result);
            
                } else {
                    reject (new Error(error));
                }
            })
                
        });

    },

    insertUser : function (email, name, password) {

        hashedPassword = hash(password);
        console.log("this is the hashed password",hashedPassword);
        
   

        var sql = "INSERT INTO user_credential (email, name, password) VALUES ?";
        values = [[email,name,hashedPassword]];

        return new Promise((resolve, reject) => {

            connection.query(sql, [values], function (error, results, fields) {
                // if (error) throw error;
                console.log('The insert query is completed', results);
                if(results){

                    resolve(true);
            
               
                } else {


                    if (error.errno == 1062) {
                        resolve (false);
                    } else {
                        reject (new Error(error));
                    }

            
                }
                
                
            });


        })
    
    },

    getUserWithId : async function(id) {

        var sql = "SELECT * FROM user_credential WHERE id = "+ mysql.escape(id);

        return new Promise((resolve, reject) => {
    
            connection.query(sql,async function(error,result){
       
                if(result){
    
                    resolve (result);
            
                } else {
                    reject (new Error(error));
                }
            })
                
        });

    },

    getSensorsOfUser : async function(userId) {

        var sql = "SELECT * FROM user_devices WHERE (device_type = 'sensor' OR device_type = 'both')  AND user_id = "+mysql.escape(userId);

        return new Promise((resolve, reject) => {
    
            connection.query(sql,async function(error,result){
       
                if(result){
    
                    resolve (result);
            
                } else {
                    reject (new Error(error));
                }
            })
                
        });

    },

    getSwitchesOfUser : async function(userId) {

        var sql = "SELECT * FROM user_devices WHERE (device_type = 'switch' OR device_type = 'both')  AND user_id = "+mysql.escape(userId);

        return new Promise((resolve, reject) => {
    
            connection.query(sql,async function(error,result){
       
                if(result){
    
                    resolve (result);
            
                } else {
                    reject (new Error(error));
                }
            })
                
        });

    },

    getDeviceName : async function(device_eui) {

        var sql = "SELECT device_name FROM user_devices WHERE device_eui = "+mysql.escape(device_eui);

        return new Promise((resolve, reject) => {
    
            connection.query(sql,async function(error,result){
       
                if(result){
    
                    resolve (result[0].device_name);
            
                } else {
                    reject (new Error(error));
                }
            })
                
        });

    },

    getDeviceEui : async function(deviceName) {

        var sql = "SELECT device_eui FROM user_devices WHERE device_name = "+mysql.escape(deviceName);

        return new Promise((resolve, reject) => {
    
            connection.query(sql,async function(error,result){
       
                if(result){
    
                    resolve (result);
            
                } else {
                    reject (new Error(error));
                }
            })
                
        });

    },

    setTimer : function (data, userId,deviceEui) {

        let sql;

         //first run time 
        if(data.frequency == 1) {

            sql = "UPDATE users.user_devices SET first_start_time = '"+ data.startTime +"', first_run_time = '"+ data.runTime +"', cut_off_value = '"+ data.cutOffVal +"', device_run_order = '"+ data.order +"' WHERE device_eui = '"+deviceEui+"'";

        } else if (data.frequency == 2) {

            sql = "UPDATE users.user_devices SET second_start_time = '"+ data.startTime +"', second_run_time = '"+ data.runTime +"', cut_off_value = '"+ data.cutOffVal +"', device_run_order = '"+ data.order +"' WHERE device_eui = '"+deviceEui+"'";
         

        }

        return new Promise((resolve, reject) => {

            connection.query(sql, function (error, results, fields) {
          
                console.log('The Update query is completed', results);

                if(results){

                    resolve(true);
            
               
                } else {

                    reject (new Error(error));
               
                }
            });
        })
    
    },

    addUserDevice : function (data, userId) {

        var sql = "INSERT INTO user_devices (device_eui, user_id, device_name, application_id, device_type) VALUES ?";

        values = [[data.deviceEui,userId,data.name,data.appId,data.deviceType]];

        return new Promise((resolve, reject) => {

            connection.query(sql, [values], function (error, results, fields) {
                // if (error) throw error;
                console.log('The insert query is completed', results);
                if(results){

                    resolve(true);
            
                } else {

                    console.log(error);
                    reject(new Error(error));

            
                }
                
                
            });


        })
    
    },

    addJob : async function(jobName,sensor_eui,switch_eui,run_time_in_seconds,hour,minutes){

        var sql = "INSERT INTO device_jobs (job_name, sensor_dev_eui, switch_dev_eui, run_time_in_seconds,hour,minutes)  VALUES ('"+jobName+"','"+sensor_eui+"','"+switch_eui+"','"+run_time_in_seconds+"','"+hour+"','"+minutes+"') ON DUPLICATE KEY UPDATE sensor_dev_eui='"+sensor_eui+"', switch_dev_eui ='"+switch_eui+"', run_time_in_seconds ='"+ run_time_in_seconds+"', hour = '"+hour+"', minutes = '"+minutes+"';"

        return new Promise((resolve, reject) => {

            connection.query(sql, function (error, results, fields) {
                if (error) throw error;
                
                if(results){

                    resolve(true);
            
                } else {

                    console.log(error);
                    reject(new Error(error));

            
                }
                
                
            });


        })
    
    },

    getJob : async function (jobName) {


        var sql = "SELECT * FROM device_jobs WHERE job_name = "+mysql.escape(jobName);


        return new Promise((resolve, reject) => {
    
            connection.query(sql,async function(error,result){

      
                if(result){
    
                    resolve (result);
            
                } else {
                    reject (new Error(error));
                }
            })
                
        });


    },

    getCutOffValue : async function(switch_dev_eui){

        var sql = "SELECT cut_off_value FROM user_devices WHERE device_eui = "+mysql.escape(switch_dev_eui);

        return new Promise((resolve, reject) => {
    
            connection.query(sql,async function(error,result){
       
                if(result){
    
                    resolve (result[0].cut_off_value);
            
                } else {
                    reject (new Error(error));
                }
            })
                
        });



    },


    getLastState : async function(switch_dev_eui){

        var sql = "SELECT last_state FROM user_devices WHERE device_eui = "+mysql.escape(switch_dev_eui);

        return new Promise((resolve, reject) => {
    
            connection.query(sql,async function(error,result){
       
                if(result){

                    if (result[0].last_state == "ON"){

                        resolve(true);

                    } else {

                        resolve(false);

                    }
    
                } else {
                    reject (new Error(error));
                }
            })
                
        });

    },

    setLastStateToON :  async function(switch_dev_eui){

        let sql = "UPDATE users.user_devices SET last_state = 'ON' WHERE device_eui = '"+switch_dev_eui+"'";

        return new Promise((resolve, reject) => {

            connection.query(sql, function (error, results, fields) {
          
                console.log('The setting of last state to ON query is completed', results);

                if(results){

                    resolve(true);
            
               
                } else {

                    reject (new Error(error));
               
                }
            });
        })


    },

    setLastStateToOFF :  async function(switch_dev_eui){

        let sql = "UPDATE users.user_devices SET last_state = 'OFF' WHERE device_eui = '"+switch_dev_eui+"'";

        return new Promise((resolve, reject) => {

            connection.query(sql, function (error, results, fields) {
          
                console.log('The setting of Last State to OFF query is completed', results);

                if(results){

                    resolve(true);
            
               
                } else {

                    reject (new Error(error));
               
                }
            });
        })


    }




}


// function trialInsert (username, address, tokenBalance) {

//     connection.connect();

//     var sql = "INSERT INTO userdata (username, address, tokenbalance) VALUES ?";
//     values = [[username,address,tokenBalance]];


//     connection.query(sql, [values], function (error, results, fields) {
//         if (error) throw error;
//         console.log('The insert query is completed', results);
//         connection.end();
//       });

// }

// trialInsert("adad","0x232131343",30.8);


//   connection.end();

// async function checkuser (username) {

//     connection.connect();

//     let response;

//     var sql = "SELECT username FROM user.userdata WHERE username = " + mysql.escape(username);

//     return new Promise((resolve, reject) => {

//         connection.query(sql,async function(error,result){
//             console.log(result);
//             if(result){
        
//                 if(result.length) {
//                     resolve (true);
//                 }else {
//                     resolve (false);
//                 }
//             } else {
//                 reject (new Error("its error"));
//             }
//         })
            
//     });

    // data  =  await connection.query(sql,async function(error,result){
    //     response = result;
    //     return response;
    // });

    // console.log(data);

    // connection.query(sql, function (error, results, fields) {
    //     if (error) throw error;
    //     console.log('The check user', results);
    //     if(results.length){
    //         console.log("user exist");
    //         response = true;
    //     }else {
    //         console.log("user does not exist");
    //         response = false;
    //     }

    //     connection.end();
    //     return response;

        
    //   });

   

// }

// async function mainFunction() {

//     see = await checkuser("sfsd");

//     console.log("this is",see);

// }

// mainFunction();

// console.log(checkuser("noushad"));

// function updateUserData (username, address, tokenBalance) {

//     connection.connect();

//     var sql = "UPDATE userdata SET address = "+ mysql.escape(address)+", tokenbalance = "+ mysql.escape(tokenBalance) +" WHERE username = "+ mysql.escape(username);
//     console.log(sql);

//     connection.query(sql, function (error, results, fields) {
//         if (error) throw error;
//         console.log('The insert query is completed', results);
//         connection.end();
//       });

// }

// updateUserData("noushad","hdyd",30.8);




// async function getAllUsers () {

//     // connection.connect();

//     let response;

//     var sql = "SELECT * FROM user.userdata";

//     return new Promise((resolve, reject) => {

//         connection.query(sql,async function(error,result){
   
//             if(result){

//                 resolve (result);
        
//             } else {
//                 reject (new Error("its error"));
//             }
//         })
            
//     });

    

    


// async function getUser (username) {

    
//     var sql = "SELECT * FROM user.userdata WHERE username = "+ mysql.escape(username);

//     return new Promise((resolve, reject) => {

//         connection.query(sql,async function(error,result){
   
//             if(result){

//                 resolve (result);
        
//             } else {
//                 reject (new Error("its error"));
//             }
//         })
            
//     });

    

    
// }


// async function mainFunction() {

//     see = await getUser("ddfd");

//     console.log("this is",see);
//     console.log(see[0]);
//     console.log(see.length);

// }

// mainFunction();