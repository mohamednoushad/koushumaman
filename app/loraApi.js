var jwt = require('./jwt');

var rp = require('request-promise');




module.exports = {

    addDevice : async function (data) {

        let jwtToken = await jwt.getApiToken();

        var options = {
            method: 'POST',
            uri: 'http://142.93.43.92:8080/api/devices',
            body: {
                    "device": {
                      "applicationID": data.appId,
                      "description": data.desc,
                      "devEUI": data.devEUI,
                      "deviceProfileID": data.devProfId,
                      "name": data.name,
                      "referenceAltitude": 0,
                      "skipFCntCheck": true
                    }
                  },
            headers: {
                'Grpc-Metadata-Authorization': jwtToken
            },
            json: true 
        };
         
        const response = await rp(options);
        return response;
  
    }
}