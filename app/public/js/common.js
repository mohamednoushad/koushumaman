


$(document).ready(function() {

    fetch('/getUserName') //function to call the user name to display on top right of dashboard
    .then( response => {
      return response;
    })
    .then(response => response.json())
    .then(userData => {
      $("#profileName").html(userData.name.charAt(0).toUpperCase() + userData.name.substr(1)); //Making starting Capital Letter of the Name
    });
  
  });