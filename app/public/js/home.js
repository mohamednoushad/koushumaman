let deviceNameChanged;
let durationChanged;
let singleDataFlag;
let graphData;

function mainfunction() {

    $("#deviceName").html(deviceNameChanged);



    if(singleDataFlag) {
  
      uniqueData();
  
    } else {
  
      fetchData();
  
    }
  
  }

  async function uniqueData(deviceName,duration,graphType) {

    let data =  {
  
      device : deviceName ? deviceName : deviceNameChanged,
      searchTime : duration ? duration : durationChanged,
      graph : graphType ? graphType : graphData
  
      }

      switch(graphData) {
        case "bat":
            unit = " %";
            break;
        case "vwc":
            unit = " %";
            break;
        case "temp":
            unit = " °C";
            break;
        case "ec":
            unit = " (dS/m)";
            break;
        case "ε":
            unit = " ";
            break;
        default:
            unit = " m3";
            break;        

      }
  
  
  
    fetch('/getSingleData',{method: "POST",
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)})
        .then( response => {
          console.log("first response of fetch",response);
          if (response.status !== 200) {
            console.log(response);
          }
          return response;
        })
        .then(response => response.json())
        .then(parsedResponse => {
          console.log("parsed response of fetch",parsedResponse);
          parsedResponse = parsedResponse;
          unpackData = (arr, key) => {
            return arr.map(obj => obj[key])
          }

          changeToLocalTime = (arr,key) => {
            return arr.map(obj=>new Date(obj[key]));
          }

  
         const firstTrace = {
            type: 'scatter',
            mode: 'lines',
            name: 'vwc',
            x: changeToLocalTime(parsedResponse, 'time'),
            y: unpackData(parsedResponse, 'value'),
            line: {color: '#17BECF'},
            
          }

        
          const data = [firstTrace];
          const layout = {
            title: graphData,
            yaxis: {
              ticksuffix: unit
            }
          };
          return Plotly.newPlot('graphs-container', data, layout,{displayModeBar: true});
        })
  
      }

  $(".category").click(function(){

      console.log("changing category");
    graphData = $(this).text();

        console.log("here", $(this).text());
      
        if(singleDataFlag) { 
      
          uniqueData() 
      
        }
      
      });

    $(".time").click(function(){

        console.log($(this).text());

   

        let inputTime = $(this).text();
        let convertedTimeinMinutes;
    
        switch(inputTime) {
          case "5 Minutes":
            convertedTimeinMinutes = 5;
            break;
          case "1 Hour":
            convertedTimeinMinutes = 60;
            break;
          case "2 Hour":
            convertedTimeinMinutes = 120;
            break;
          case "1 Day":
            convertedTimeinMinutes = 1440;
            break;
          case "10 Days":
            convertedTimeinMinutes = 14400;
            break;
          case "1 Month":
            convertedTimeinMinutes = 43800;
            break;
          case "1 Year":
            convertedTimeinMinutes = 525600;
            break;
    
          default:
          convertedTimeinMinutes =5;
          break;
        
        }

        console.log(convertedTimeinMinutes);
    
        durationChanged = convertedTimeinMinutes;
    
        console.log("calling drop down of time");
    
        if(singleDataFlag) { 
    
          uniqueData() 
    
        } else {
    
          fetchData();
    
        }
    
     });

     function getData(_this) {

        deviceName = $(_this).attr("value");
        deviceNameChanged = $(_this).attr("value");
        durationChanged = 5 ;

        $("#deviceName").html(deviceNameChanged);
    
        if(singleDataFlag) {
    
          uniqueData(deviceName,durationChanged,graphData);
      
        } else {
      
          fetchData(deviceName,durationChanged);
      
        }
    
      }


$(document).ready(function() {

    fetch('/listSensors')
    .then( response => {
      return response;
    })
    .then(response => response.json())
    .then(parsedResponse => {
         
      parsedResponse.forEach(element => {
        $(".collapse-header-sensors").append('<a class="collapse-item" value="'+element.device_name+'" id="'+element.device_name+'" onclick="getData(this)" >'+element.device_name+'</a>');
      });


    deviceNameChanged = parsedResponse.length ? parsedResponse[0].device_name : null;
    console.log(deviceNameChanged);
      if(deviceNameChanged == null) {
        $('#noDevicesModal').modal('show');
      } else {
      
      durationChanged = 5;
      graphData = "vwc";
      singleDataFlag = true;
      mainfunction();
      setInterval(mainfunction,20*1000);

      }


    });

});