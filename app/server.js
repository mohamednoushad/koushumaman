const Influx = require('influx');
const hash = require('sha1');
const uuid = require('uuid/v4');
const timelite = require('timelite/time');
const session = require('express-session')
const FileStore = require('session-file-store')(session);
var rp = require('request-promise');
var jwt = require('./jwt');
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const influx = new Influx.InfluxDB('http://142.93.43.92:8086/soil_sensor');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const mysqlDb = require('./mysql');
const loraApi = require('./loraApi');

const jobScheduleApiUrl = "http://142.93.43.92:4200"; //for server
//const jobScheduleApiUrl = "http://localhost:4200";  //for local

let timeZoneOffset = "04:00:00"; //change it to  4 hours for UAE



// configure passport.js to use the local strategy
passport.use(new LocalStrategy(
  { usernameField: 'email' },
  async function(email, password, done) {

    try {

      const res = await mysqlDb.getUser(email);
    
      const user = res[0];
      // console.log("this is user from db",user);
      // console.log("this is user input password",password);
      // console.log("this is user input password hash",hash(password));
      console.log(hash(password));
      console.log(!user);
    
      if (!user) {
        return done(null, false, { message: 'Invalid credentials.' });
      }
      if (!(hash(password) == user.password)) {
        return done(null, false, { message: 'Invalid credentials.' });
      }
      return done(null, user);

    }catch(e){
      console.log(e);
    }
    
  
  }
));

// tell passport how to serialize the user
passport.serializeUser((user, done) => {
    console.log('Inside serializeUser callback. User id is save to the session file store here')
    done(null, user.id);
  });

passport.deserializeUser((id, done) => {

 console.log("deserialize",id);
  mysqlDb.getUserWithId(id)
    .then(res => done(null, res[0]) )
    .catch(error => done(error, false))
 });

  

 

app.use(session({
    genid: (req) => {
      console.log('Inside the session middleware')
      console.log(req.sessionID)
      return uuid() // use UUIDs for session IDs
    },
    store: new FileStore(),
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: true
  }))

app.use(passport.initialize());
app.use(passport.session());

app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.set('view engine', 'ejs')


app.set('port', 4100);

influx.getMeasurements()
  .then(names => console.log('My measurement names are: ' + names.join(', ')))
  .then(() => {
    app.listen(app.get('port'), () => {
      console.log(`Listening on ${app.get('port')}.`);
    });
  })
  .catch(error => console.log({ error }));



app.get('/devices',(request,response) => {

    influx.query(`select distinct("device_name") from (select "value", "device_name" from device_frmpayload_data_e25)`)
    .then(result => {
          
        let devices= [];

        result.forEach(element => {

            devices.push(element.distinct);

        });

        response.status(200).json(devices);
    })
    .catch(error => response.status(500).json({ error }));

})


app.post('/api', async function(request, response) {


    console.log(request.body);

    let table;
    let queryTimeInMinutes = request.body.searchTime;
    let deviceName = request.body.device;

    let typesOfData = ["e25","bat","ec","temp","vwc","ka"];
    let colors = ["#00008B","#8B008B","#000000","#800000","#006400","#FF4500"];

    try {

    let data = [];

    for (const i in typesOfData) {

        switch(typesOfData[i])  {

            case "e25" : 
                table = "device_frmpayload_data_e25";
                break;
    
            case "bat" :
                table = "device_frmpayload_data_bat";
                break;
    
            case "ec" :
                table = "device_frmpayload_data_ec";
                break;
    
            case "temp" :
                table = "device_frmpayload_data_temp";
                break;
             
            case "vwc" :
                table = "device_frmpayload_data_e25";
                break;
    
               
        }

        console.log(table);

        if (typesOfData[i] == "vwc") {

             result = await influx.query(`SELECT ((-0.053+(0.0292*value))-(0.00055*value*value)+(4.3*0.000001*value*value*value))*100 AS value_ka FROM `+ table +` where time > now() - `+queryTimeInMinutes+`m AND "device_name" = '`+ deviceName +`'`);

            trace = {
                type: 'scatter',
                mode: 'lines',
                name: typesOfData[i],
                x: result.map(a => a.time),
                y: result.map(a => a.value_ka),
                line: {color: colors[i]}
              }

        } else {

             result = await influx.query(`SELECT * FROM `+ table +` where time > now() - `+queryTimeInMinutes+`m AND "device_name" = '`+ deviceName +`'`);

             trace = {
                type: 'scatter',
                mode: 'lines',
                name: typesOfData[i],
                x: result.map(a => a.time),
                y: result.map(a => a.value),
                line: {color: colors[i]}
              }

        }

     


        data.push(trace);
                
    }

    response.status(200).json(data); 

    } catch(e) {

        console.log(e);
        res.send(e);

    }
   
});

app.post('/getSingleData', (request, response) => {

    console.log(request.body);

    let table;
    let queryTimeInMinutes = request.body.searchTime;
    let deviceName = request.body.device;

    switch(request.body.graph)  {

        case "ε" : 
            table = "device_frmpayload_data_e25";
            break;

        case "bat" :
            table = "device_frmpayload_data_bat";
            break;

        case "ec" :
            table = "device_frmpayload_data_ec";
            break;

        case "temp" :
            table = "device_frmpayload_data_temp";
            break;

        case "valve" :
            table = "device_frmpayload_data_valv";
            break;

        case "vwc" :
            table = "device_frmpayload_data_e25";
            break;

        case "ka" :
            table = "device_frmpayload_data_vwc";
            break;
        case "Valve" :
            table = "device_frmpayload_data_valv";
            break;
        case "Battery" :
            table = "device_frmpayload_data_bat";
            break;

            

    }

    if(request.body.graph == "vwc") {

      influx.query(`SELECT ((-0.053+(0.0292*value))-(0.00055*value*value)+(4.3*0.000001*value*value*value))*100 AS value FROM `+ table +` where time > now() - `+queryTimeInMinutes+`m AND "device_name" = '`+ deviceName +`'`)
      .then(result => response.status(200).json(result))
      .catch(error => response.status(500).json({ error }));

    }else {

      influx.query(`SELECT * FROM `+ table +` where time > now() - `+queryTimeInMinutes+`m AND "device_name" = '`+ deviceName +`'`)
    .then(result => response.status(200).json(result))
    .catch(error => response.status(500).json({ error }));

    }

    
});



app.get('/', (request, response) => {

    console.log(request.sessionID)

    response.render('index');

});


app.get('/irigatepro', (request, response) => {

  response.render('irigatepro');

});

app.get('/registration', (request, response) => {

    response.render('registration');

});


// create the login get and post routes
app.get('/login', (req, res) => {
    console.log('Inside GET /login callback function')
    console.log(req.sessionID)
    res.render('login')
  })
  
  app.post('/login', (req, res, next) => {
    passport.authenticate('local', (err, user, info) => {
      if(info) {return res.send(info.message)}
      if (err) { return next(err); }
      if (!user) { return res.redirect('/login'); }
      req.login(user, (err) => {
        if (err) { return next(err); }
        return res.redirect('/authrequired');
      })
    })(req, res, next);
  })
  

  app.get('/authrequired', (req, res) => {
    if(req.isAuthenticated()) {

          res.render("home");

     } else {

      res.redirect('/login');

    }
  })

  app.get('/logout', function (req, res){
    req.session.destroy(function (err) {
      res.redirect('/'); //Inside a callback… bulletproof!
    });
  });

  app.post('/createUser', async function (req, res, next) {

    console.log(req.body);

    data = req.body;

    try {

      createUser = await mysqlDb.insertUser( data.email, data.name, data.password);

      console.log("this is create user result here",createUser);

      if (createUser) {
        res.status(200).send("user created successfully");
      } else {
        res.status(200).send("user already exist");
      }

 

    } catch (e) {

      console.log(e);

      res.send(e);

    }
 })

 app.get('/listSensors',async function(req,res){

  userId = req.session.passport.user;

  const devices = await mysqlDb.getSensorsOfUser(userId);

  console.log(devices);

  res.status(200).send(devices);

 })

 app.get('/getUserName',async function(req,res){

  userId = req.session.passport.user;

  const userDetails = await mysqlDb.getUserWithId(userId);
  console.log(userDetails[0].name);

  res.status(200).send({name:userDetails[0].name});

 })

 app.get('/listSwitches',async function(req,res){

  userId = req.session.passport.user;

  const devices = await mysqlDb.getSwitchesOfUser(userId);

  console.log(devices);

  res.status(200).send(devices);

 })

 app.get('/switchesView', function(req,res){
   res.render('switchDashboard');
 })

 app.get('/switchSettings', function(req,res){
  res.render('switchSettings');
})

app.post('/setTimer',async function(req,res){

  userId = req.session.passport.user;

  data = req.body;

 try {

  const deviceEui = await mysqlDb.getDeviceEui(data.device);


  if(userId) {

    let userInputTime = data.startTime;

    let convertedUTCTimeForJob = timelite.sub([userInputTime,timeZoneOffset]);

    let timeToSave = convertedUTCTimeForJob.join(':'); 

    const result = await mysqlDb.setTimer(data,timeToSave,deviceEui[0].device_eui);

    rp(jobScheduleApiUrl+'/resheduleJobs');
    
    if(result) {

      res.status(200).send({message:"success"});

    }

  } else {

    console.log("user id not found");

  }



} catch(e) {

  console.log(e);
  res.send(e);

}



 })

app.get('/addDevice', function(req,res){
  res.render('addDevice');
})

app.post('/device',async function(req,res){

      userId = req.session.passport.user;

      data = req.body;

    try {


      if(userId) {

        const result = await loraApi.addDevice(data);

        console.log("here",result);
        
        if(result) {

          const addToDb  = mysql.addUserDevice(data,userId);

          console.log(addToDb);

          if(addToDb) {

            res.status(200).send({message:"success"});

          } else {
            console.log(error,addToDb);
          }

        

        }

      } else {

        console.log("user id not found");

      }



    } catch(e) {

      console.log(e);
      res.send(e);

    }



 })