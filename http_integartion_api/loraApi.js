var jwt = require('./jwt');

var rp = require('request-promise');


let api_url = "http://142.93.43.92:8080/api";


async function getDevice(devEui) {


    let jwtToken = await jwt.getApiToken();

    var options = {
        method: 'GET',
        uri: api_url+'/devices/'+devEui,
        headers: {
            'Grpc-Metadata-Authorization': jwtToken
        },
        json: true 
    };
     
    const response = await rp(options);
    console.log("this is get device data response",response);
    return response;

}


module.exports = {

    getDeviceData: async function (devEui) {

        let jwtToken = await jwt.getApiToken();

        var options = {
            method: 'GET',
            uri: api_url+'/devices/'+devEui,
            headers: {
                'Grpc-Metadata-Authorization': jwtToken
            },
            json: true 
        };
         
        const response = await rp(options);
        console.log("this is get device data response",response);
        return response;
  
    },

    isSensorActive : async function(devEui) {

        const deviceData = await getDevice(devEui);

        lastSeen = Date.parse(deviceData.lastSeenAt);
        let presentTime = new Date().getTime();
        let checkTime = presentTime - 2 * 60 * 1000 ; //considering check time two minutes ago to see if device is active;
        if(lastSeen > checkTime ){
            //console.log("device is actively giving values");
            return true;
        } else {
            //console.log("sensor is not actively giving values");
            return false;
        }


    },

    openSwitch :  async function(devEui) {

        let jwtToken = await jwt.getApiToken();

        var options = {
            method: 'POST',
            uri: api_url+'/devices/'+devEui+'/queue',
            body : {
                "deviceQueueItem": {
                  "confirmed": true,
                  "data": "AwE=",
                  "devEUI": devEui,
                  "fCnt": 0,
                  "fPort": 1,
                  "jsonObject": ""
                }
              },
            headers: {
                'Grpc-Metadata-Authorization': jwtToken
            },
            json: true 
        };
         
        const response = await rp(options);


        return response;
    },

    closeSwitch :  async function(devEui) {

        let jwtToken = await jwt.getApiToken();

        var options = {
            method: 'POST',
            uri: api_url+'/devices/'+devEui+'/queue',
            body : {
                "deviceQueueItem": {
                  "confirmed": true,
                  "data": "AwA=",
                  "devEUI": devEui,
                  "fCnt": 0,
                  "fPort": 1,
                  "jsonObject": ""
                }
              },
            headers: {
                'Grpc-Metadata-Authorization': jwtToken
            },
            json: true 
        };
         
        const response = await rp(options);

        return response;
    },


}